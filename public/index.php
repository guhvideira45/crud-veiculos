<?php
ob_start();

require __DIR__ . "/vendor/autoload.php";

/**
 * BOOTSTRAP
 */

use Source\Core\Session;
use CoffeeCode\Router\Router;

$session = new Session();
$route = new Router(url(), ":");
$route->namespace("Source\App");
$route->group("/");


//CRUD
$route->get("/", "App:home");
$route->get("/{search}/{page}", "App:home");
$route->get("/create", "App:create");
$route->get("/view/{id}", "App:view");

$route->post("/filter", "App:home");
$route->post("/create", "App:save");
$route->post("/update", "App:save");
$route->post("/delete", "App:delete");

/** ERROR ROUTER */
$route->group("/");
$route->namespace("Source\App")->group("/ooops");
$route->get("/{errcode}", "Error:error");


/** ROUTER */
$route->dispatch();

/** ERROR REDIRECT */
if ($route->error()) {
    $route->redirect("/ooops/{$route->error()}");
}

ob_end_flush();
