<?php
/**
 * DATABASE
 */
define("CONF_DB_HOST", "mysql");
define("CONF_DB_USER", "app");
define("CONF_DB_PASS", "app");
define("CONF_DB_NAME", "crud-veiculos");



/**
 * PROJECT URLs
 */
define("CONF_URL_BASE", "http://localhost:8080");
define("CONF_URL_DEV", "http://localhost:8080");

/**
 * SITE
 */
define("CONF_SITE_NAME", "CRUD");
define("CONF_SITE_LANG", "pt_BR");



/**
 * DATES
 */
define("CONF_DATE_BR", "d/m/Y H:i:s");
define("CONF_DATE_APP", "Y-m-d H:i:s");


/**
 * PASSWORD
 */
define("CONF_PASSWD_MIN_LEN", 8);
define("CONF_PASSWD_MAX_LEN", 40);
define("CONF_PASSWD_ALGO", PASSWORD_DEFAULT);
define("CONF_PASSWD_OPTION", ["cost" => 10]);


/**
 * VIEW
 */
define("CONF_VIEW_PATH", __DIR__ . "/../../themes");
define("CONF_VIEW_THEME_DEFAULT", "");
define("CONF_VIEW_THEME_ADMIN", "admin");
define("CONF_VIEW_EXT", "php");


/**
 * UPLOAD
 */
define("CONF_UPLOAD_DIR", "storage");
define("CONF_UPLOAD_IMAGE_DIR", "images");
define("CONF_UPLOAD_FILE_DIR", "files");
define("CONF_UPLOAD_MEDIA_DIR", "medias");


/**
 * IMAGES
 */
define("CONF_IMAGE_CACHE", CONF_UPLOAD_DIR . "/" . CONF_UPLOAD_IMAGE_DIR . "/cache");
define("CONF_IMAGE_SIZE", 2000);
define("CONF_IMAGE_QUALITY", ["jpg" => 75, "png" => 5]);


/**
 * MAIL
 */

// GMAIL
define("CONF_MAIL_HOST", "smtp.gmail.com");
define("CONF_MAIL_PORT", "587");
define("CONF_MAIL_USER", "gm.rhsystem@gmail.com");
define("CONF_MAIL_PASS", "mhi7CpWsKP9N2Fm");
define("CONF_MAIL_SENDER", ["name" => "CONTATO - G&M Sistemas", "address" => "gm.rhsystem@gmail.com"]);
define("CONF_MAIL_SUPPORT", "guhvideira45@gmail.com");
define("CONF_MAIL_OPTION_LANG", "br");
define("CONF_MAIL_OPTION_HTML", true);
define("CONF_MAIL_OPTION_AUTH", true);
define("CONF_MAIL_OPTION_SECURE", "tls");
define("CONF_MAIL_OPTION_CHARSET", "utf-8");


define("CONF_AUTH_EXPIRE", 30);