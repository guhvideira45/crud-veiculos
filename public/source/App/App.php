<?php

namespace Source\App;

use Source\Core\Controller;
use Source\Models\Vehicle;
use Source\Support\Pager;

/**
 * Class App
 * @package Source\Core\Controller
 */
class App extends Controller
{
	/**
	 * Class constructor
	 */
	public function __construct()
	{
		parent::__construct(__DIR__ . "/../../themes/" . CONF_VIEW_THEME_DEFAULT . "/");
	}

	/**
	 * @param array|null $data
	 * @return void
	 */
	public function home(?array $data): void
	{
		//SEARCH REDIRECT
		if(isset($data["s"])) {
			if (!empty($data["s"])) {
				$s = str_search($data["s"]);
				$json["redirect"] = url("/{$s}/1");
				echo json_encode($json);
				return;
			} else {
				$json["redirect"] = url("/");
				echo json_encode($json);
				return;
			}
		}

		$search = null;
		$vehicles = (new Vehicle())->find("deleted_at IS NULL");

		if (!empty($data["search"]) && str_search($data["search"]) != "all") {
			$search = str_search($data["search"]);
			$vehicles = (new Vehicle())->find("MATCH(plate, manufacturer, model) AGAINST(:s) AND deleted_at IS NULL", "s={$search}");
			if (!$vehicles->count()) {
				$this->message->info("Sua pesquisa não retornou resultados")->flash();
				redirect("/");
			}
		}

		$all = ($search ?? "all");
		$pager = new Pager(url("/{$all}/"));
		$pager->pager($vehicles->count(), 5, (!empty($data["page"]) ? $data["page"] : 1), 2);

		echo $this->view->render("vehicles/index", [
			"head" => (object)[
                "title" => CONF_SITE_NAME . " | Listagem de Veículos",
            ],
			"search" => $search,
			"vehicles" => $vehicles->order("id")->limit($pager->limit())->offset($pager->offset())->fetch(true),
			"paginator" => $pager->render()
		]);
	}

	/**
	 * @param array|null $data
	 * @return void
	 */
	public function create(): void
	{
		echo $this->view->render("vehicles/vehicle", [
			"head" => (object)[
                "title" => CONF_SITE_NAME . " | Incluir Veículo",
            ],
			"vehicle" => null,
			"manufacturers" => (new Vehicle())->find("manufacturer IS NOT NULL AND deleted_at IS NULL", null, "DISTINCT manufacturer AS value")->fetch(true),
			"models" => (new Vehicle())->find("model IS NOT NULL AND deleted_at IS NULL", null, "DISTINCT model AS value")->fetch(true),
			"colors" => (new Vehicle())->find("color IS NOT NULL AND deleted_at IS NULL", null, "DISTINCT color AS value")->fetch(true)
		]);
	}

	/**
	 * @param array $data
	 * @return void
	 */
	public function view(array $data): void
	{

		$id = filter_var(($data["id"] ?? 0), FILTER_VALIDATE_INT);
		$vehicle = (new Vehicle())->findById($id);
		if (!$vehicle) {
			$this->message->error("Você tentou gerenciar um veículo que não existe")->flash();
			redirect("/");
		}

		echo $this->view->render("vehicles/vehicle", [
			"head" => (object)[
                "title" => CONF_SITE_NAME . " | Alterar Veículo",
            ],
			"vehicle" => $vehicle,
			"manufacturers" => (new Vehicle())->find("id = :id OR ( manufacturer IS NOT NULL AND deleted_at IS NULL )", "id='{$vehicle->id}'", "DISTINCT manufacturer AS value")->fetch(true),
			"models" => (new Vehicle())->find("id = :id OR ( model IS NOT NULL AND deleted_at IS NULL )", "id='{$vehicle->id}'", "DISTINCT model AS value")->fetch(true),
			"colors" => (new Vehicle())->find("id = :id OR ( color IS NOT NULL AND deleted_at IS NULL )", "id='{$vehicle->id}'", "DISTINCT color AS value")->fetch(true)
		]);
	}

	/**
	 * @param array $data
	 * @return void
	 */
	public function save(array $data): void
	{
		$data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

		$vehicle = new Vehicle();
		if (!empty($id = filter_var(($data["id"] ?? 0), FILTER_VALIDATE_INT))) {
			$vehicle = (new Vehicle())->findById($id);
			if (!$vehicle) {
				$json["message"] = $this->message->error("Você tentou gerenciar um cadastro que não existe")->toastr();
				echo json_encode($json);
				return;
			}
		}

		$vehicle->plate = strtoupper($data["plate"] ?? null);
		$vehicle->manufacturer = strtoupper($data["manufacturer"] ?? null);
		$vehicle->model = strtoupper($data["model"] ?? null);
		$vehicle->year = filter_var(($data["year"] ?? null), FILTER_VALIDATE_INT);
		$vehicle->year_model = filter_var(($data["year_model"] ?? null), FILTER_VALIDATE_INT);
		$vehicle->color = strtoupper($data["color"] ?? null);
		$vehicle->notes = !empty($data["notes"]) ? $data["notes"] : null;
		$vehicle->status = (!empty($data["status"]) && in_array($data["status"], ["active", "inactive"]) ? $data["status"] : "inactive");

		if (!$vehicle->save()) {
			$json["message"] = $vehicle->message()->toastr();
			echo json_encode($json);
			return;
		}

		if (!empty($id)) {
			$this->message->success("Cadastro atualizado com sucesso.")->flash();
			$json["reload"] = true;
		} else {
			$this->message->success("Veículo incluido com sucesso. Confira...")->flash();
			$json["redirect"] = url("/view/{$vehicle->id}");
		}

		echo json_encode($json);
		return;
	}

	/**
	 * @param array|null $data
	 * @return void
	 */
	public function delete(array $data): void
	{
		$data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

		$id = filter_var(($data["id"] ?? 0), FILTER_VALIDATE_INT);
		$vehicle = (new Vehicle())->findById($id);

		if (!$vehicle) {
			$json["message"] = $this->message->error("Você tentou excluir um veículo que não existe")->toastr();
			echo json_encode($json);
			return;
		}

		$vehicle->destroy();
		$this->message->success("Cadastro excluido com sucesso...")->flash();
		$json["redirect"] = url("/");
		echo json_encode($json);
		return;
	}
}
