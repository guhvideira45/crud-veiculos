<?php

namespace Source\Models;

use Source\Core\Model;

/**
 * MAGUS | Class Vehicle Active Record Pattern
 *
 * @author Gustavo H. Videira Martins <gustavo.videira@outlook.com>
 * @package Source\Models
 */
class Vehicle extends Model
{
  /**
   * Vehicle constructor.
   */
  public function __construct()
  {
    parent::__construct("vehicles", ["id"], ["plate", "manufacturer", "model", "year", "year_model", "color", "status"]);
  }

  /**
   * @return null|Vehicle
   */
  public function save(): bool
  {
    if(!$this->required()) {
      $this->message->warning("Placa, fabricante, modelo, ano de fabricação, ano do modelo e cor são obrigatórios");
      return false;
    }

    if(!preg_match('/[A-Za-z]{3}-[0-9]{4}/', $this->plate) && !preg_match('/[A-Za-z]{3}-[0-9][A-Za-z][0-9]{2}/', $this->plate)) {
      $this->message->warning("A placa informada não tem um formato válido");
      return false;
    }

    /** Vehicle Update */
    if(!empty($this->id)) {
      $vehicleId = $this->id;

      if($this->find("plate = :p AND id != :i AND deleted_at IS NULL", "p={$this->plate}&i={$vehicleId}", "id")->fetch()) {
        $this->message->warning("A placa informada já está cadastrado");
        return false;
      }

      $this->update($this->safe(), "id = :id", "id={$vehicleId}");
      if($this->fail()) {
        $this->message->error("Erro ao atualizar, verifique os dados");
        return false;
      }
    }

    /** Vehicle Create */
    if (empty($this->id)) {
      if($this->find("plate = :p AND deleted_at IS NULL", "p={$this->plate}", "id")->fetch()) {
        $this->message->warning("A placa informada já está cadastrado");
        return false;
      }

      $vehicleId = $this->create($this->safe());
      if ($this->fail()) {
        $this->message->error("Erro ao cadastrar, verifique os dados");
        return false;
      }
    }

    $this->data = ($this->findById($vehicleId))->data();
    return true;
  }
}
