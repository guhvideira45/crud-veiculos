<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?= $head->title ?? CONF_SITE_NAME; ?></title>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
		<?= $v->section("styles"); ?>
		<link rel="stylesheet" href="<?= theme("/assets/style.css"); ?>" />
		<link rel="icon" type="image/png" href="<?= theme("/assets/images/favicon.png"); ?>" />
	</head>
	<body class="sidebar-mini layout-top-nav"> <!-- PARA USAR O DARK-MODE BASTA ADICIONAR A CLASSE dark-mode NO BODY -->
		<div class="wrapper">
			<div class="ajax_load">
				<div class="ajax_load_box">
					<div class="ajax_load_box_circle"></div>
					<p class="ajax_load_box_title">Aguarde, carregando...</p>
				</div>
			</div>
			<nav class="main-header navbar navbar-expand-md navbar-dark">
				<div class="container">
					<a href="<?= url("/") ?>" class="navbar-brand">
						<img src="<?= theme("assets/img/AdminLTELogo.png"); ?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
						<span class="brand-text font-weight-light">CRUD</span>
					</a>
				</div>
			</nav>

			<!-- Content -->
			<div class="content-wrapper">
				<?= $v->section("content"); ?>
			</div>
		</div>

		<script src="<?= theme("/assets/scripts.js"); ?>"></script>
		<script>
			$(".select2").select2();
			
			$("#daterange").daterangepicker({
				locale: {
					format: "DD/MM/YYYY",
					applyLabel: "Aplicar",
					cancelLabel: "Cancelar",
					fromLabel: "De",
					toLabel: "Até",
					customRangeLabel: "Personalizar",
					daysOfWeek: [
						"Dom",
						"Seg",
						"Ter",
						"Qua",
						"Qui",
						"Sex",
						"Sab"
					],
					monthNames: [
						"Janeiro",
						"Fevereiro",
						"Março",
						"Abril",
						"Maio",
						"Junho",
						"Julho",
						"Agosto",
						"Setembro",
						"Outubro",
						"Novembro",
						"Dezembro"
					]
				},
				//autoApply: true,
				// maxDate: moment(),
				startDate: moment().subtract(30, 'days'),
				endDate: moment()
			}, function(start, end, label) {
  			console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
			});
		</script>
		<?= $v->section("scripts"); ?>
	</body>
</html>
