<?php $v->layout("_theme"); ?>

<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-8">
				<?php if(!$vehicle): ?>
					<h1 class="m-0"><i class="fas fa-sm fa-plus-circle"></i> Incluir</h1>
				<?php else: ?>
					<h1 class="m-0"><i class="fas fa-sm fa-car"></i> <?= $vehicle->plate; ?></h1>
				<?php endif; ?>
			</div>
			<div class="col-sm-4">
				<a href="<?= url("/"); ?>" class="btn font-weight-bold float-right" ><i class="fas fa-arrow-left"></i> Voltar</a>
			</div>
		</div>
	</div>
</section>
<section class="content">
	<div class="container-fluid">
		<div class="row"><div class="col-sm-12"><?= flash(); ?></div></div>
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<form action="<?= url("/create"); ?>" method="post">
						<!--ACTION SPOOFING-->
						<?php if(!$vehicle): ?>
							<input type="hidden" name="action" value="create" />
						<?php else: ?>
							<input type="hidden" name="action" value="update" />
							<input type="hidden" name="id" value="<?= ($vehicle->id ?? ""); ?>" />
						<?php endif; ?>
						<div class="card-body">
							<div class="row">
								<div class="col-sm-12 col-md-4">
									<div class="form-group">
										<label>*Placa:</label>
										<input type="text" name="plate" class="form-control mask-plate" placeholder="Placa do veículo" value="<?= ($vehicle->plate ?? ""); ?>" required>
									</div>
								</div>
								<div class="col-sm-12 col-md-4">
									<div class="form-group">
										<label>*Fabricante:</label>
										<?php if(!empty($manufacturers)): ?>
											<select class="form-control select2" id="manufacturer" name="manufacturer" style="width: 100%;" required>>
												<option value="" selected disabled>Selecione uma opção</option>
												<?php foreach($manufacturers as $manufacturer): ?>
													<option value="<?= ucfirst($manufacturer->value); ?>" <?= (($vehicle->manufacturer ?? "") == $manufacturer->value ? "selected" : ""); ?>><?= ucfirst($manufacturer->value); ?></option>
												<?php endforeach; ?>
												<option value="*">Outro</option>
											</select>
										<?php else: ?>
										<input type="text" name="manufacturer" class="form-control" placeholder="Fabricante" value="<?= ($vehicle->manufacturer ?? ""); ?>" required>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-sm-12 col-md-4">
									<div class="form-group">
										<label>*Modelo:</label>
										<?php if(!empty($models)): ?>
											<select class="form-control select2" id="model" name="model" style="width: 100%;" required>>
												<option value="" selected disabled>Selecione uma opção</option>
												<?php foreach($models as $model): ?>
													<option value="<?= ucfirst($model->value); ?>" <?= (($vehicle->model ?? "") == $model->value ? "selected" : ""); ?>><?= ucfirst($model->value); ?></option>
												<?php endforeach; ?>
												<option value="*">Outro</option>
											</select>
										<?php else: ?>
											<input type="text" name="model" class="form-control" placeholder="Modelo" value="<?= ($vehicle->model ?? ""); ?>" required>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-sm-12 col-md-4">
									<div class="form-group">
										<label>*Ano fabricação:</label>
										<input type="number" name="year" class="form-control" placeholder="Ano de fabricação" min="0" value="<?= ($vehicle->year ?? ""); ?>" required>
									</div>
								</div>
								<div class="col-sm-12 col-md-4">
									<div class="form-group">
										<label>*Ano modelo:</label>
										<input type="number" name="year_model" class="form-control" placeholder="Ano do modelo" min="0" value="<?= ($vehicle->year_model ?? ""); ?>" required>
									</div>
								</div>
								<div class="col-sm-12 col-md-4">
									<div class="form-group">
										<label>*Cor:</label>
										<?php if(!empty($colors)): ?>
											<select class="form-control select2" id="color" name="color" style="width: 100%;" required>>
												<option value="" selected disabled>Selecione uma opção</option>
												<?php foreach($colors as $color): ?>
													<option value="<?= ucfirst($color->value); ?>" <?= (($vehicle->color ?? "") == $color->value ? "selected" : ""); ?>><?= ucfirst($color->value); ?></option>
												<?php endforeach; ?>
												<option value="*">Outro</option>
											</select>
										<?php else: ?>
											<input type="text" name="color" class="form-control" placeholder="Cor" value="<?= ($vehicle->color ?? ""); ?>" required>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-sm-8">
									<div class="form-group">
										<label>Observações:</label>
										<textarea name="notes" class="form-control" rows="3" placeholder="Observações complementares do veículo..."><?= ($vehicle->notes ?? ""); ?></textarea>
									</div>
								</div>
								<div class="col-sm-12 col-md-4">
									<div class="form-group">
										<label>Status:</label>
										<select class="form-control" name="status">
											<option value="active" <?= (($vehicle->status ?? "") == "active" ? "selected" : ""); ?>>Ativo</option>
											<option value="inactive" <?= (($vehicle->status ?? "") == "inactive" ? "selected" : ""); ?>>Inativo</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<?php if ($vehicle): ?>
								<a href="#" class="btn font-weight-bold text-danger" data-post="<?= url("/delete"); ?>" data-action="delete" data-confirm="ATENÇÃO: Tem certeza que deseja excluir o veículo e todos os dados relacionados a ele? Essa ação não pode ser feita!" data-id="<?= $vehicle->id; ?>"><i class="fas fa-exclamation-triangle"></i> Excluir Veículo</a>
							<?php endif; ?>
							<button type="submit" class="btn btn-primary float-right" id="btn-submit"><i class="fas fa-check-square"></i> Salvar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
			
	</div>
</section>

<?php $v->start("scripts"); ?>
<script>
	<?php if(!empty($manufacturers)): ?>
	$("#manufacturer").on("select2:select", function(e) {
		if($("#manufacturer").val() === "*") {
			$("#manufacturer").parent().html(`<label>*Fabricante:</label>
			<input type="text" name="manufacturer" class="form-control" placeholder="Fabricante" required>`);
		}
	});
	<?php endif; ?>
	
	<?php if(!empty($models)): ?>
	$("#model").on("select2:select", function(e) {
		if($("#model").val() === "*") {
			$("#model").parent().html(`<label>*Modelo:</label>
			<input type="text" name="model" class="form-control" placeholder="Modelo" required>`);
		}
	});
	<?php endif; ?>

	<?php if(!empty($colors)): ?>
	$("#color").on("select2:select", function(e) {
		if($("#color").val() === "*") {
			$("#color").parent().html(`<label>*Cor:</label>
			<input type="text" name="color" class="form-control" placeholder="Cor" required>`);
		}
	});
	<?php endif; ?>
</script>
<?php $v->end(); ?>
