<?php $v->layout("_theme"); ?>

<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-8">
				<h1 class="m-0"><i class="fas fa-sm fa-car"></i> Veículos</h1>
			</div>
			<div class="col-sm-4">
				<a href="<?= url("/create"); ?>" class="btn btn-primary font-weight-bold float-right" ><i class="fas fa-plus-circle"></i> Incluir</a>
			</div>
		</div>
	</div>
</section>
<section class="content">
	<div class="container-fluid">
		<div class="row"><div class="col-sm-12"><?= flash(); ?></div></div>
		<div class="card card-solid">
			<?php if(!empty($vehicles)): ?>
				<div class="card-header">
					<h3 class="card-title">Veículos</h3>
					<div class="card-tools">
						<form method="post" action="<?= url("/filter") ?>" enctype="multipart/form-data">
							<div class="input-group input-group-sm" style="width: 300px;">
								<input type="text" name="s" class="form-control float-right" placeholder="Pesquisar..." <?= (!empty($search) ? "value=\"{$search}\"" : ""); ?>>
								<div class="input-group-append">
									<button type="submit" class="btn btn-default">
										<i class="fas fa-search"></i>
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="card-body table-responsive p-0">	
					<table class="table text-nowrap">
						<thead>
							<tr>
								<th>#</th>
								<th>Placa</th>
								<th>Fabricante</th>
								<th>Modelo</th>
								<th>Status</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($vehicles as $vehicle): ?>
							<tr>
								<td><?= $vehicle->id; ?></td>
								<td><?= $vehicle->plate; ?></td>
								<td><?= $vehicle->manufacturer; ?></td>
								<td><?= $vehicle->model; ?></td>
								<td><span class="badge <?= ($vehicle->status == "active" ? "bg-success" : "bg-secondary"); ?>"><?= ($vehicle->status == "active" ? "Ativo" : "Inativo"); ?></span></td>
								<td>
									<a href="<?= url("/view/{$vehicle->id}"); ?>" class="btn btn-primary" title="Alterar cadastro"><i class="fa fa-pencil-alt"></i></a>
									<a href="#" class="btn btn-danger" title="Excluir cadastro" data-post="<?= url("/delete"); ?>" data-action="delete" data-confirm="ATENÇÃO: Tem certeza que deseja excluir o veículo e todos os dados relacionados a ele? Essa ação não pode ser feita!" data-id="<?= $vehicle->id; ?>"><i class="fa fa-trash"></i></button>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>
				<?php if($paginator): ?>
					<div class="card-footer">
						<nav>
							<?= $paginator; ?>
						</nav>
					</div>
				<?php endif; ?>
			<?php else: ?>
				<div class="card-body">
					<div class="row">
						<div class="col-sm-12 text-center">
							<h4>Não existem veículos cadastrados, <a href="<?= url("/create"); ?>">clique aqui</a> para incluir um novo!</h4>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>