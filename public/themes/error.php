<?php $v->layout("_theme"); ?>

<section class="content-header">
	<div class="container-fluid"></div>
</section>
<section class="content">
	<div class="error-page">
		<h2 class="text-secondary text-center" style="font-size: 8rem; font-weight: 350;"><?= $error->code; ?></h2>
		<div class="error-content ml-0">
			<h3 class="text-center"><i class="fas fa-exclamation-triangle"></i> <?= $error->title; ?></h3>
			<p><?= $error->message; ?></p>
			<?php if(!empty($error->link)): ?>
				<div class="row">
					<div class="col-sm-12 col-md-4 offset-md-4">
						<a href="<?= $error->link; ?>" class="btn btn-primary btn-block"><?= $error->linkTitle; ?></a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
