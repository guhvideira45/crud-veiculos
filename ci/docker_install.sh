#!/bin/bash

#We need to install dependecies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doens't have it) which is required by composer
# Also ensure that pcre headers are available; they seem to have disappeared
apt-get update -yqq
apt-get install git zlib1g-dev libpcre3-dev -yqq

# Install mysql driver
# Here you can install any other extension that you need
docker-php-ext-install pdo_mysql
# docker-php-ext-install zip