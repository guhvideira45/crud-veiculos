#!/bin/bash

# NOTE: *: ${VAR:=DEFAULT_VALUE}" is the same as "VAR=${VAR:-DEFAULT_VALUE}"

: ${VERSION_ROOT:=/var/www}
: ${SCP_HOST:=www.example.com}
: ${SCP_USER:=$USER}
: ${SCRIPT_NAME:=ci/$(basename $0)}

# If there's a specific override for the host for this environment, use ir
SCP_HOST_VAR=SCP_HOST_${CI_ENVIRONMENT_NAME}
SCP_HOST=${!SCP_HOST_VAR:-$SCP_HOST}

function prefix () {
	while read l ; do
		echo $1 $l
	done
}

if [ "x$1" != "x" ]; then
	echo "Running remote commands"

	NEW_VERSION_HASH=$1

	echo "Deploying app from /tmp/$NEW_VERSION_HASH.tar.bz2 to $VERSION_ROOT/$NEW_VERSION_HASH"

	# Second, cd to the root of the app
	cd $VERSION_ROOT
	mkdir $NEW_VERSION_HASH
	cd $NEW_VERSION_HASH

	if [ $? -ne 0 ]; then
		echo "Unable to change directory to root of app"
		exit 1
	fi

	tar jxf /tmp/$NEW_VERSION_HASH.tar.bz2

	echo $ENVIORNMENT > .environment

	# Third, run composer
	composer install
	pails migrate

	cd ..
	rm -f current && ln -sf $NEW_VERSION_HASH current

	rm /tmp/$NEW_VERSION_HASH.tar.bz2

	exit 0
fi

echo "Running local commands"

# scp or copy the files over to the remote machine
THIS_VERSION_HASH=$(git rev-parse --short HEAD)
scp -q $SCRIPT_NAME $SCP_USER@SCP_HOST:/tmp/$THIS_VERSION_HASH-$(basename $SCRIPT_NAME)
scp -q $THIS_VERSION_HASH.tar.bz2 $SCP_USER@SCP_HOST:/tmp/
if [ $? -ne 0 ]; then
	echo "Upload FAILED"
	exit 1
fi
echo "Upload SUCCEEDED"

# Kick off installation on the remote machine
ssh $SCP_USER@SCP_HOST VERSION_ROOT=$VERSION_ROOT ENVIORNMENT=$CI_ENVIRONMENT_NAME bash /tmp/$THIS_VERSION_HASH-$(basename $SCRIPT_NAME) $THIS_VERSION_HASH 2>&1 | prefix REMOTE:
if [ $? -ne 0 ]; then
	echo "Deployment FAILED"
	exit 1
fi

exit 0
