CREATE DATABASE IF NOT EXISTS `crud-veiculos`;
USE `crud-veiculos`;

-- --------------------------------------------------------
--
-- Estrutura para tabela `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(10) UNSIGNED NOT NULL,
  `plate` varchar(8) NOT NULL,
  `manufacturer` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `year` int(11) NOT NULL,
  `year_model` int(11) NOT NULL,
  `color` varchar(50) NOT NULL,
  `notes` text,
  `photo` varchar(255) DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'active' COMMENT 'active, inactive',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `vehicles`
--
ALTER TABLE `vehicles` ADD PRIMARY KEY (`id`);
ALTER TABLE `vehicles` ADD FULLTEXT KEY `plate_manufacturer_model` (`plate`,`manufacturer`,`model`);


--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;